let data = [
  {
    id: 1,
    image:
      "https://hinhnen4k.com/wp-content/uploads/2023/02/anh-gai-xinh-vn-3.jpg",
    description: "MASSA AST<br>Color : black , Material : mental",
    quantity: 1,
    price: 120.0,
    discount: 25.0,
  },
  {
    id: 2,
    image:
      "https://hinhnen4k.com/wp-content/uploads/2023/02/anh-gai-xinh-vn-3.jpg",
    description: "MASSA AST<br>Color : black , Material : mental",
    quantity: 1,
    price: 7.0,
    discount: 0,
  },
  {
    id: 3,
    image:
      "https://hinhnen4k.com/wp-content/uploads/2023/02/anh-gai-xinh-vn-3.jpg",
    description: "MASSA AST<br>Color : black , Material : mental",
    quantity: 1,
    price: 120.0,
    discount: 25.0,
  },
];

const renderData = (data) => {
  let element = "";
  let totalPrice = 0;
  let totalDiscount = 0;
  let totalTax = 0;
  element += `<tr>
            <th>Product</th>
            <th>Description</th>
  <th>Quantity</th>
  <th>Price</th>
  <th>Dicount</th>
  <th>Tax</th>
  <th>Total</th>
    </tr>`;
  data.forEach((product) => {
    const { image, description, price, discount, quantity, id } = product;
    const tax = calculateTax(quantity, price);
    const total = calculateTotal(quantity, price, discount, tax);
    totalPrice += total;
    totalTax += tax;
    totalDiscount += discount;
    element += `
            <tr>
                <td>
                    <img style="width: 100px;" src="${image}" />
                </td>  
                <td>${description}</td> 
                <td>
                    <div class="form-inline">
                            <input type="text" class="form-control" readonly="" value="${quantity}">
                            <button type="button" class="tru" onclick="handleDecreaseQuantity(${id})">-</button>
                            <button type="button" class="cong" onclick="handleIncreaseQuantity(${id})">+</button>
                            <button type="button" class="xoa" onclick="handleCancel(${id})">x</button>
                    </div>
                </td>
                <td>$${price.toFixed(2)}</td>
                <td>$${discount.toFixed(2)}</td>
                <td>$${tax.toFixed(2)}</td>
                <td>$${total.toFixed(2)}</td>
            </tr>
        `;
  });
  element += `<tr>
    <td colspan="6" class="design">Total Price: </td> 
    <td>$${totalPrice.toFixed(2)}</td>
    </tr>
<tr>
    <td colspan="6" class="design">Total Discount:</td>
    <td>$${totalDiscount.toFixed(2)}</td>
    </tr>
    <tr>
    <td colspan="6" class="design">Total Tax:</td>
    <td>$${totalTax.toFixed(2)}</td>
    </tr>`;
  $("#table").html(element);
};

const calculateTax = (quantity, price) => {
  const tax = quantity * price * 0.125;
  const roundedTax = Math.ceil(tax);
  return roundedTax;
};

const calculateTotal = (quantity, price, discount, tax) => {
  const total = quantity * price - discount + tax;
  return total;
};

const handleDecreaseQuantity = (id) => {
  const productToUpdate = data.find((p) => p.id === id);
  if (productToUpdate && productToUpdate.quantity > 1) {
    productToUpdate.quantity -= 1;
  }
  renderData(data);
};

const handleIncreaseQuantity = (id) => {
  const productToUpdate = data.find((p) => p.id === id);
  if (productToUpdate) {
    productToUpdate.quantity += 1;
  }
  renderData(data);
};

const handleCancel = (id) => {
  data = data.filter((item) => item.id !== id);
  renderData(data);
};

renderData(data);
