
const countrylist = ["Vietnam", "Singapore", "Malaysia", "Indonesia", "Philippine", "Lao", "Cambodia"];
const positionlist = ["Operator", "Manager", "Developer", "Designer", "Tester"];

const countrydropdown = document.getElementById("country");
const defaultcountry = document.createElement("option");
defaultcountry.text = "Select country";
defaultcountry.disabled = true;
defaultcountry.selected = true;
countrydropdown.appendChild(defaultcountry);
countrylist.forEach(function (country) {
    let option = document.createElement("option");
    option.value = country;
    option.text = country;
    countrydropdown.appendChild(option);
});

const positiondropdown = document.getElementById("position");
const defaultposition = document.createElement("option");
defaultposition.text = "Choose desired position";
defaultposition.disabled = true;
defaultposition.selected = true;
positiondropdown.appendChild(defaultposition);
positionlist.forEach(function (position) {
      let option = document.createElement("option");
      option.value = position;
      option.text = position;
      positiondropdown.appendChild(option);
});  
const validateInput = (event) =>{
    event.preventDefault();
    clearAllErrorText();
    checkBlankFirstName();
    checkBlankLastName();
    checkBlankEmail();
    validateEmail();
    validatePhone();
    checkSelectedCountry();
    checkSelectedPosition();
}

const checkBlankFirstName = () =>{
    let firstName = document.getElementById("firstName").value;
    if(firstName == ""){
        document.getElementById("errFirstName").innerHTML = "The First Name should not be blank";
    }
}
const checkBlankLastName = () =>{
    let firstName = document.getElementById("lastName").value;
    if(firstName == ""){
        document.getElementById("errLastName").innerHTML = "The Last Name should not be blank";
    }
}
const checkBlankEmail = () =>{
    let firstName = document.getElementById("email").value;
    if(firstName == ""){
        document.getElementById("errEmail").innerHTML = "The Email should not be blank";
    }
}
const validateEmail = () => {
        let email = document.getElementById("email").value;
        if (email != "") {
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailRegex.test(email)) {
                document.getElementById("errEmail").innerHTML = "Please input your correct email address";
            } 
        }
    }
const validatePhone = () =>{
    let phone = document.getElementById("phone").value;
    const phoneRegex = /^\d{3}-\d{3}-\d{7}$/;
    if(!phoneRegex.test(phone)){
        document.getElementById("errPhone").innerHTML = "Please input your correct phone number";
    }
}
const checkSelectedCountry = () =>{
    let country = document.getElementById("country").value;
    if(country == ""){
        document.getElementById("errCountry").innerHTML = "Please select your country";
    }
}
const checkSelectedPosition = () =>{
    let position = document.getElementById("position").value;
    if(position == ""){
        document.getElementById("errPosition").innerHTML = "Please select your position";
    }
}
const clearAllErrorText = () =>{
    document.getElementById("errFirstName").innerHTML = "";
    document.getElementById("errLastName").innerHTML = "";
    document.getElementById("errEmail").innerHTML = "";
    document.getElementById("errPhone").innerHTML = "";
    document.getElementById("errCountry").innerHTML = "";
    document.getElementById("errCity").innerHTML = "";
    document.getElementById("errAddress").innerHTML = "";
    document.getElementById("errPosition").innerHTML = "";
}