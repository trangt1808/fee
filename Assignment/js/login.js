$(document).ready(function () {
    var userData= getCookie("login");
    if(!userData){
        return;
    }
    userData= JSON.parse(userData);
    $("#email").val(userData.email);
    $("#password").val(userData.password);

    //save data user to cookie when login
    $("#login").on("click", function () {
        var isChecked = $("#Remember").prop("checked");
        if(isChecked){
            var data={
                email: $("#email").val(),
                password: $("#password").val()
            }
            setCookie("login", JSON.stringify(data), 7);
        } else{
            var data={
                email: $("#email").val(),
            }
            setCookie("login", JSON.stringify(data), 7);
        }
    })
})


function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var cookies = document.cookie.split(';');

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1, cookie.length);
        }

        if (cookie.indexOf(nameEQ) === 0) {
            return cookie.substring(nameEQ.length, cookie.length);
        }
    }
    return null;
}
